# ACME Sports Teams Views JSON Proof

This is a tiny proof to display an API endpoint in Drupal. There are a lot of ways this could be done, but I've chosen two for this challenge.

The first pass of the challenge I quickly reviewed the issue and decided to build it with a VIEWS approach. It's the easiest and offers the most front-end flexibility and takes up no time to implement successfully showcasing Drupal's power. But the point of the task is to showcase my programming ability, so I've offered a second approach to allow for better gauging of this. The programmatic way is more work but attempts to provide the same output as the VIEWS but completely done leveraging Drupal's APIs and framework. Though it may not have the same front-end flexibility it does offer the coder more back-end power.

This proof does not have any back-end caching but could be extended to include some with a bit more work.

## Instructions:

Install the module and enable it. If all goes well, you should be able to render the page directly as I've coded the API into the config yml file and it will set this value on install. Should the value need to be changed you can find it here:

- `/admin/config/content/acme_sports`

To see the data rendered, there are two possible approaches.

1. The programmatic way
1. Or using VIEWS

I believe in the KISS principle, and thus, I much prefer the VIEWS approach as it simplifies the approach but also makes it infinitely customizable from a content editor perspective. Whereas if we go the programmatic route, I sort of lock the site into what I think the output should be.

## Using VIEWS

- surf to the `/football` path.

## Using the programmatic approach

- surf to `/football-teams` path.
