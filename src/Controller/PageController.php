<?php

namespace Drupal\acme_sports\Controller;

use Drupal;
use Drupal\acme_sports\TeamsManagerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Utility\TableSort;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller routines for acme sports teams.
 */
class PageController extends ControllerBase {

  /**
   * @var \Drupal\acme_sports\TeamsManagerInterface
   */
  private TeamsManagerInterface $acme_sports_manager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $controller = new static(
      $container->get('acme_sports.manager')
    );
    $controller->setStringTranslation($container->get('string_translation'));
    return $controller;
  }

  /**
   * PageController constructor.
   *
   * @param \Drupal\acme_sports\TeamsManagerInterface $acme_sports_manager
   */
  public function __construct(TeamsManagerInterface $acme_sports_manager) {
    $this->acme_sports_manager = $acme_sports_manager;
  }

  /**
   * The teams rendering method.
   */
  public function teams() {

    $this->acme_sports_manager->buildTeamsTable();
    $results = $this->acme_sports_manager->getApiResponseObject('results');
    $teams = $results['data'];

    // TODO: Could place these in the settings page so we can "manually" select
    //       the column headers.
    $columns = ['division', 'name', 'display_name', 'nickname'];

    $header = [];
    foreach($columns as $name) {
      $header[] = ['data' => ucfirst($this->t($results['columns'][$name])), 'field' => $name];
    }

    $rows = [];
    $conferences = [];
    $rows = $this->sort_header_rows($teams['team'], $header);
    foreach ($rows as $row) {
      $row_data = [];
      // Normally we would add some nice formatting to our rows
      // but for our purpose we are simply going to add our row
      // to the array.
      foreach($columns as $key) {
        $row_data[] = $row[$key];
      }

      $conferences[$row['conference']][] = ['data' => $row_data];
    }

    // Build the table for nice output.
    $build = [
      '#markup' => '<p>' . t('The content on this page is plombed from an API call and rendered using a controller.') . '</p>',
    ];
    foreach($conferences as $conference_name => $conference_rows) {
      $build['tablesort_table_' . hash('md5', json_encode($conference_rows))] = [
        '#theme' => 'table',
        '#caption' => $conference_name,
        '#header' => $header,
        '#rows' => $conference_rows,
        '#empty' => t('There is no data available.'),
      ];
    }

    return $build;
  }

  /**
   * Sort table rows
   *
   * @param $rows
   *  Rows which need sorting
   * @param $header
   *  Table header
   * @param int|string $flag
   *
   * @return array
   */
  function sort_header_rows($rows, $header, $flag = SORT_STRING|SORT_FLAG_CASE): array {
    $order = TableSort::getOrder($header, Drupal::request());
    $sort = TableSort::getSort($header, Drupal::request());
    $column = $order['sql'];
    foreach ($rows as $row) {
      $temp_array[] = $row[$column];
    }
    if ($sort == 'asc') {
      asort($temp_array, $flag);
    }
    else {
      arsort($temp_array, $flag);
    }

    foreach ($temp_array as $index => $data) {
      $new_rows[] = $rows[$index];
    }

    return $new_rows;
  }
}
