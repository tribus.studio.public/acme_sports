<?php

namespace Drupal\acme_sports;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\TranslationManager;
use GuzzleHttp\Client as GuzzleClient;

/**
 * Class TeamsManager.
 */
class TeamsManager implements TeamsManagerInterface {

  /**
   * @var string
   */
  private string $config_settings = 'acme_sports.settings';

  /**
   * @var array
   */
  protected array $settings;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Drupal\Core\StringTranslation\TranslationManager definition.
   *
   * @var \Drupal\Core\StringTranslation\TranslationManager
   */
  protected TranslationManager $stringTranslation;

  /**
   * @var array
   */
  public array $apiResponseObject;

  /**
   * Constructs a new ScoresManager object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\StringTranslation\TranslationManager $string_translation
   */
  public function __construct(ConfigFactoryInterface $config_factory, TranslationManager $string_translation) {
    $this->configFactory = $config_factory;
    $this->stringTranslation = $string_translation;
    $this->settings = $this->configFactory->getEditable($this->config_settings)->getRawData();
  }

  /**
   * @inheritDoc
   */
  public function getConfigFactory(): ConfigFactoryInterface {
    return $this->configFactory;
  }

  /**
   * @inheritDoc
   * @throws \Exception
   */
  public function buildTeamsTable() {
    $this->getApiResponse();
//    $this->createTable();
  }

  public function getApiResponse() {
    if (!$this->getApiSettings('api_uri')) {
      throw new \Exception('API is missing some settings ');
    }

    $client = new GuzzleClient();
    $url = $this->getApiSettings('api_uri');
    $response = $client->get($url);
    if ($response->getStatusCode() !== 200 && $response->getStatusCode() !== 304) {
      throw new \Exception(
        'API failed to load, got response code ' . $response->getStatusCode()
      );
    }
    $this->setApiResponseObject(Json::decode($response->getBody()->__toString()));
  }

  public function getApiSettings($key = NULL) {
    $setting = FALSE;
    if ($key !== NULL) {
      try {
        $setting = $this->settings[$key];
      }
      catch (\Exception $e) {
        return FALSE;
      }
      return $setting;
    }
    else {
      return $this->settings;
    }
  }

  /**
   * The a key from the config factory
   *
   * @param $key
   *
   * @return mixed|void
   */
  public function getConfigKey($key) {
    $config = $this->getConfigFactory()->get($this->config_settings);
    return $config->get($key);
  }

  /**
   * @inheritDoc
   */
  public function setApiResponseObject(array $object) {
    $this->apiResponseObject = $object;
  }

  /**
   * @inheritDoc
   */
  public function getApiResponseObject($key) {
     if (!empty($key)) {
       return $this->apiResponseObject[$key];
     }
  }

}
