<?php

namespace Drupal\acme_sports;

/**
 * Interface TeamsManagerInterface.
 */
interface TeamsManagerInterface {

  /**
   * @return mixed
   */
  public function getConfigFactory();

  /**
   * Get ACME API response.
   *
   * @return mixed
   */
  public function getApiResponse();

  /**
   * Get the resulting values from the array by key!
   *
   * @param $key
   *
   * @return mixed
   */
  public function getApiResponseObject($key);

  /**
   * @param string $key
   *
   * @return mixed
   */
  public function getApiSettings($key = NULL);

  /**
   * @param array $object
   *
   * @return mixed
   */
  public function setApiResponseObject(array $object);

  /**
   * @param $key
   *
   * @return mixed
   */
  public function getConfigKey($key);

  /**
   * Builds the data object from the API JSON.
   *
   * @return mixed
   */
  public function buildTeamsTable();
}
